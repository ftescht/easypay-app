import React, { Component } from 'react';
import './style.css';

import {
  checkClientId, getClientId,
  checkClientSecret, getClientSecret,
  authorize
} from '../../utils/auth';

class Login extends Component {
  state = {
    locked: false,
    clientId: getClientId() || '',
    clientSecret: getClientSecret() || ''
  };

  onClientIdChange = (e) => {
    e.preventDefault();
    const { clientId } = this.state;
    const { value } = e.target;
    if (value !== clientId) {
      this.setState({
        clientId: value
      });
    }
  };

  onClientSecretChange = (e) => {
    e.preventDefault();
    const { clientSecret } = this.state;
    const { value } = e.target;
    if (value !== clientSecret) {
      this.setState({
        clientSecret: value
      });
    }
  };

  onLogin = (e) => {
    e.preventDefault();
    const { clientId, clientSecret } = this.state;
    authorize(clientId, clientSecret);
  };

  render() {
    const { locked, clientId, clientSecret } = this.state;
    return (
      <div className='Login'>
        <h2>OAuth App - Login</h2>
        <form className='pure-form pure-form-stacked'>
          <input type='text' placeholder='Client ID' value={clientId} onChange={this.onClientIdChange} disabled={locked}/>
          <input type='text' placeholder='Client Secret' value={clientSecret} onChange={this.onClientSecretChange} disabled={locked}/>
          <button type='submit' className='pure-button pure-button-primary' onClick={this.onLogin} disabled={locked || !(checkClientId(clientId) && checkClientSecret(clientSecret))}>Sign in</button>
        </form>
      </div>
    );
  }
}

export default Login;
