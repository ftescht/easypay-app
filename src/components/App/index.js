import React, { Component } from 'react';
import { isMobilePhone } from 'validator';

import 'purecss/build/tables.css';
import './style.css';

import net from '../../utils/net';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: [],
      loading: false,
      saving: false,
      phones: [],
      phone: '',
      valid: true
    };
  }

  componentDidMount() {
    this.onRefresh();
  }

  onRefresh = () => {
    this.setState({ loading: true });
    net('/phones')
      .then(phones => {
        return ({ phones });
      })
      .catch(error => ({ errors: this.state.errors.concat([error]) }))
      .then(state => {
        state.loading = false;
        this.setState(state);
      });
  };

  onRemove = (e, phone) => {
    e.preventDefault();
    const result = window.confirm('Remove phone '+ phone +'?');
    if (result) {
      net('/phoneRemove', { phone })
        .then(() => {
          this.onRefresh();
        })
        .catch(error => {
          this.setState({
            errors: this.state.errors.concat([error]),
            loading: false
          });
        });
    }
  };

  onAdd = (e) => {
    e.preventDefault();
    const { phone } = this.state;
    this.setState({ saving: true });
    net('/phoneAdd', { phone })
      .then(() => {
        this.setState({ phone: '', saving: false });
        this.onRefresh();
      })
      .catch(error => {
        this.setState({
          errors: this.state.errors.concat([error]),
          saving: false
        });
      })
  };

  onPhoneChange = (e) => {
    e.preventDefault();
    const { phone } = this.state;
    const value = e.target.value.replace(/\D/g, '');
    if (value !== phone) {
      if (value) {
        this.setState({ phone: value, valid: isMobilePhone(value, 'ru-RU') });
      } else {
        this.setState({ phone: value, valid: true });
      }
    }
  };

  onRemoveError = (e, errorIndex) => {
    e.preventDefault();
    const { errors } = this.state;
    this.setState({ errors: errors.slice(0, errorIndex).concat(errors.slice(errorIndex + 1)) });
  };

  render() {
    const { loading, saving, phones, phone, valid } = this.state;
    const $errors = this.state.errors.map((error, errorIndex) => {
      return <div key={'error-'+ errorIndex} className='errorMsg' onClick={e => this.onRemoveError(e, errorIndex)}>{error.toString()}</div>
    });

    const $phones = [];
    phones.forEach((phone, phoneIndex) => {
      $phones.push(
        <tr key={'phone-'+ phone +'-'+ phoneIndex}>
          <td className='phone'>{phone}</td>
          <td className='phone-remove'>
            <button className='pure-button button-warning' disabled={loading} onClick={e => this.onRemove(e, phone)}>Remove</button>
          </td>
        </tr>
      )
    });

    return (
      <div className='App'>
        <h1>OAuth App - Phones</h1>
        <form className='pure-form'>
          <input className={'addInput'+ (valid ? '' : ' invalid')} type='text' placeholder='Phone' value={phone} onChange={this.onPhoneChange} />
          <button disabled={saving || !(phone && valid)} className='addBtn pure-button button-success' onClick={this.onAdd}>Add</button>
        </form>
        {$errors}
        <table className={'pure-table pure-table-bordered phones'+ (loading ? ' loading' : '')}>
          <tbody>{$phones}</tbody>
        </table>
      </div>
    );
  }
}

export default App;
