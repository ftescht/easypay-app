import { getToken, clearAuth, requestToken } from './utils/auth';
import { ping } from './utils/net';

import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';

import 'purecss/build/base.css';
import 'purecss/build/forms.css';
import 'purecss/build/buttons.css';
import './index.css';


import App from './components/App';
import Login from './components/Login';


const $appRoot = document.getElementById('root');

function renterApp() {
  ReactDOM.render(<App/>, $appRoot);
}
function renderLogin() {
  clearAuth();
  ReactDOM.render(<Login />, $appRoot);
}

if (getToken()) {
  ping()
    .then(renterApp)
    .catch(renderLogin);
} else {
  const code = (new URL(document.location)).searchParams.get('code');
  if (code) {
    requestToken(code)
      .catch(() => true)
      .then(() => window.location = '/')
  } else {
    renderLogin();
  }
}

registerServiceWorker();
