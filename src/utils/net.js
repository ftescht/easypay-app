import { getToken } from './auth';

import { API_URL, API_ROUTE } from '../config';

function _request(url, data) {
  const token = getToken();
  if (!token) {
    return Promise.reject(new Error('Empty token'));
  }
  const params = {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    }
  };
  if (data) {
    params.body = JSON.stringify(data);
  }
  return fetch(API_URL + API_ROUTE + url, params)
    .catch(error => {
      console.error('API ERROR', error);
      return Promise.reject(error);
    });
}

export default function netJSON(url, data) {
  return _request(url, data).then(res => res.json()).then(res => {
    if (!res.ok) return Promise.reject(new Error(res.error || 'Empty error'));
    return res.result;
  })
}

export function ping() {
  return _request('/ping').then(res => res.status === 200 ? +res.text() : Promise.reject('Bad request'));
}
