import {
  APP_NAME, APP_URL, API_URL,
  CLIENT_ID_SIZE, CLIENT_SECRET_SIZE, CLIENT_TOKEN_SIZE
} from '../config';

const ls = window.localStorage;

const CLIENT_ID_KEY = APP_NAME +'_CLIENT_ID';
const CLIENT_SECRET_KEY = APP_NAME +'_CLIENT_SECRET';
const TOKEN_KEY = APP_NAME +'_TOKEN';


export function checkClientId(clientId) {
  return clientId && clientId.length === CLIENT_ID_SIZE;
}
export function getClientId() {
  return ls.getItem(CLIENT_ID_KEY);
}
export function setClientId(clientId) {
  if (checkClientId(clientId)) return ls.setItem(CLIENT_ID_KEY, clientId);
  throw new Error('Bad client id');
}
export function removeClientId() {
  ls.removeItem(CLIENT_ID_KEY);
}


export function checkClientSecret(clientSecret) {
  return clientSecret && clientSecret.length === CLIENT_SECRET_SIZE;
}
export function getClientSecret() {
  return ls.getItem(CLIENT_SECRET_KEY);
}
export function setClientSecret(clientSecret) {
  if (checkClientSecret(clientSecret)) return ls.setItem(CLIENT_SECRET_KEY, clientSecret);
  throw new Error('Bad client secret');
}
export function removeClientSecret() {
  ls.removeItem(CLIENT_SECRET_KEY);
}


export function checkToken(token) {
  return token && token.length === CLIENT_TOKEN_SIZE;
}
export function getToken() {
  return ls.getItem(TOKEN_KEY);
}
export function setToken(token) {
  if (checkToken(token)) return ls.setItem(TOKEN_KEY, token);
  throw new Error('Bad token');
}
export function removeToken() {
  ls.removeItem(TOKEN_KEY);
}



export function clearAuth() {
  // removeClientId();
  // removeClientSecret();
  removeToken();
}

export function authorize(clientId, clientSecret) {
  if (checkClientId(clientId) && checkClientSecret(clientSecret)) {
    clearAuth();
    setClientId(clientId);
    setClientSecret(clientSecret);
    window.location = API_URL + '/oauth/authorization?client_id='+ encodeURI(clientId) +'&redirect_uri='+ encodeURI(APP_URL) +'&response_type=code';
  }
}

export function requestToken(code) {
  if (!code) return Promise.reject(new Error('Bad auth code'));
  const clientId = getClientId();
  if (!checkClientId(clientId)) return Promise.reject(new Error('Bad auth clientId'));
  const clientSecret = getClientSecret();
  if (!checkClientSecret(clientSecret)) return Promise.reject(new Error('Bad auth clientSecret'));

  const params = {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Basic '+ btoa(clientId +':'+ clientSecret)
    },
    body: JSON.stringify({
      'grant_type': 'authorization_code',
      'redirect_uri': APP_URL,
      'code': code,
      'client_id': clientId
    })
  };
  return fetch(API_URL +'/oauth/token', params)
    .then(res => res.json())
    .then(data => {
      if (data && data.access_token && checkToken(data.access_token)) {
        setToken(data.access_token);
        return true;
      }
    });
}

