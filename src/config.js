export const APP_NAME = 'oauth_app';
export const APP_URL = window.location.origin;

export const API_URL = 'http://easypay-service.ftescht.ru';
export const API_ROUTE = '/api';

export const CLIENT_ID_SIZE = 16;
export const CLIENT_SECRET_SIZE = 32;
export const CLIENT_TOKEN_SIZE = 256;
